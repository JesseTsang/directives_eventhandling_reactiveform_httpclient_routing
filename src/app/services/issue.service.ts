import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class IssueService {
  private _url: string = "http://localhost:3000/issues";
  private id;

  constructor(private http: HttpClient) {
    
   }

  /**
   * Implement addIssue method using HttpClient for a saving a Issue details
   * @param issue 
   * @returns 
   */
  addIssue(issue): Observable<any> {
    this.getIssues(); // stupid hack to get around the id problem now

    let payload =  {
      "title": issue.title,
      "description": issue.description,
      "id": this.id + 1
    };

    let payloadJSON = JSON.stringify(payload);

    if (issue != null) {
      const headers = { 'content-type': 'application/json' };

      let result = this.http.post(this._url, payloadJSON, { 'headers': headers });

      return result;
    }
    
    return null;
  }

  getIssues(): Observable<any> {
    let result = this.http.get(this._url);
    
    let dataPack = result.subscribe(data => {
      this.id = Object.keys(data).length;

      console.log("ID latest: " + this.id);
    });

    return result;
  }

  // Implement deleteIssue method to delete a issue by id
  deleteIssue(id: any): Observable<any> {
    
    return this.http.delete(this._url + "/" +  id);
  }
}
