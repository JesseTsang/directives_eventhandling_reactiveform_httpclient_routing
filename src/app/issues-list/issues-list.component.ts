import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Issue } from '../models/Issue';
import { IssueService } from '../services/issue.service';

@Component({
  selector: 'app-issues-list',
  templateUrl: './issues-list.component.html',
  styleUrls: ['./issues-list.component.css']
})
export class IssuesListComponent implements OnInit {
  // issuesList is to store all Issue data
  issuesList: Issue[] = [];

  constructor(private issueService: IssueService, private router: Router) { }

  // Write logic to get all issues from IssueService
  ngOnInit() {
    this.issueService.getIssues().subscribe(
      data => {
        this.issuesList = data;
        console.log("From issues-list.components.ts: " + data);
      }
    );
  }

  // Implement deleteIssue method to delete the issue
  deleteIssue(issueId) {
    this.issueService.deleteIssue(issueId).subscribe( data => {
      console.log(data);
      this.ngOnInit();
    });
  }

}
