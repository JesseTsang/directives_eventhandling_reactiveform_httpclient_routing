import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IssueService } from '../services/issue.service';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.css']
})
export class IssueComponent implements OnInit {
  
  form = new FormGroup({
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required)
  });

  // message to be display if Issue added or not
  message = '';

  // showMessage is for validating contact is added or not
  showMessage;

  // Form is created in html file and write code to make it functional using FormBuilder
  // Write logic to make all fields as mandatory

  constructor(private issueService: IssueService, private router: Router) {
    this.showMessage = false;
  }

  ngOnInit() {
  }

  // Implement onSubmit method to save a Issue, verify form is valid or not
  // Display message 'Title and Description should not be empty!!! Please verify details' if form is invalid
  // Display message 'Failed to add Issue!!' while error handling
  // Display message 'Issue added' if Issue is added
  onSubmit() {
    if (this.form.controls['title'].value === '' || this.form.controls['description'].value === '') {
      this.message = 'Title and Description should not be empty!!! Please verify details';
      this.showMessage = true;
      return;
    }

    this.issueService.addIssue(this.form.value)
                    .subscribe(data => {
                      console.log (data);
                      this.message = "Issue added";
                      this.showMessage = true;
                      this.clearForm();
                      this.router.navigate([`issueslist`]);
                    });   
  }

  // clearForm method is to reset the form after submitting
  clearForm() {
    this.form = new FormGroup({
      title: new FormControl(''),
      description: new FormControl('')
    });

    this.showMessage = false;
  }

}
